import re, sys
from bs4 import BeautifulSoup
import pandas as pd
from urllib.request import urlopen as uReq
from urllib.request import Request as req
import urllib
from datetime import date

daftar_wp = pd.read_csv(sys.argv[1])
pages = int(input("How many pages you would like to parse?"))
file_name = input("Please name the output file (e.g.file_1): ")
title = []
desc = []
link = []
list_wp = daftar_wp['NAMA_WP']

class Scrape:
        def __init__(self, term):
                self.term = re.sub('[?|$|.|!]', '', term)
                quoted = urllib.parse.quote(term)
                self.url = 'https://www.google.com/search?q={0}'.format(quoted)
        def run(self):
                global title
                global desc
                global link
                title.append(str(self.term))
                desc.append("------")
                link.append("------")
                headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'}
                for i in range(pages):
                        request = req(url = self.url, headers = headers)
                        my_request = uReq(request)
                        read = my_request.read()
                        soup = BeautifulSoup(read, "html.parser")
                        container_r = soup.findAll("div", {"class":"r"})
                        container_s = soup.findAll("span", {"class":"st"})

                        for j in range(len(container_r)):
                                link.append(container_r[j].a["href"])
                                title.append(container_r[j].h3.text)
                                desc.append(container_s[j].text)
                             
                        footer = soup.find("div",{"id":"foot"})
                        td = footer.findAll("a",{"class":"fl"})
                        self.url='https://www.google.com{0}'.format(td[i]["href"])


for i in list_wp:
        obj= Scrape(i)
        obj.run()

data={'Title': title, 'Description': desc, "URLs":link}
df=pd.DataFrame(data=data)
df.index+=1
df.to_excel(file_name+".xlsx")
print("File "+file_name+".xlsx is created")


